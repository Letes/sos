<?php
//phpinfo();


include "log.php";
$ruta='../../../../tmp/errorCalibrator.log';
error($ruta,'201',"Start Calibrator");

$url = 'http://sos.irceline.be/sos';
$timeEnd = date('c');
$timeStart = date("c", strtotime ( '-1 hour' , strtotime ( $timeEnd ) ));
error($ruta,'202',"Time start: " .$timeStart );
error($ruta,'203',"Time end: " .$timeEnd );
$recalibrate = false;
$error_percentage_measures = 0.1;
$distanceToStations=1500;

$stationsMeasuresName = array(
    "42101 - CO",
    "42102 - CO2",
    "42601 - NO",
    "42602 - NO2",
    "44201 - O3",
    "89101 - PM1",
    "81104 - PM2.5",
    "81102 - PM10",
    "61110 - WSP-SCA",
    "62101 - TT");

var_dump($stationsMeasuresName, true);

$GetObservationStationTimeHeader='<?xml version="1.0" encoding="UTF-8"?>'.
    '<GetObservation xmlns="http://www.opengis.net/sos/1.0"'.
    'xmlns:ows="http://www.opengis.net/ows/1.1"'.
    'xmlns:gml="http://www.opengis.net/gml"'.
    'xmlns:ogc="http://www.opengis.net/ogc"'.
    'xmlns:om="http://www.opengis.net/om/1.0"'.
    'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'.
    'xsi:schemaLocation="http://www.opengis.net/sos/1.0'.
    'http://schemas.opengis.net/sos/1.0.0/sosGetObservation.xsd"'.
    'service="SOS" version="1.0.0" srsName="urn:ogc:def:crs:EPSG::4326">';

$GetObservationStationTimeMeasurements='<!-- offering ID -->'.
    '<offering>42101 - CO</offering>'.
    '<offering>42102 - CO2</offering>'.
    '<offering>42601 - NO</offering>'.
    '<offering>42602 - NO2</offering>'.
    '<offering>44201 - O3</offering>'.
    '<offering>89101 - PM1</offering>'.
    '<offering>81104 - PM2.5</offering>'.
    '<offering>81102 - PM10</offering>'.
    '<offering>61110 - WSP-SCA</offering>'.
    '<offering>62101 - TT</offering>';

$GetObservationStationTimeTimeInterval='<!-- time interval -->'.
    '<eventTime>'.
    '<ogc:TM_During>'.
    '<ogc:PropertyName>om:samplingTime</ogc:PropertyName>'.
    '<gml:TimePeriod>'.
    '<gml:beginPosition>'.$timeStart.'</gml:beginPosition>'.
    '<gml:endPosition>'.$timeEnd.'</gml:endPosition>'.
    '</gml:TimePeriod>'.
    '</ogc:TM_During>'.
    '</eventTime>';

$GetObservationStationTimeHeaderMeasurementsProperty='<!-- saroad code -->'.
    '<observedProperty>44201 - O3</observedProperty>'.
    '<observedProperty>42102 - CO2</observedProperty>'.
    '<observedProperty>89101 - PM1</observedProperty>'.
    '<observedProperty>81104 - PM2.5</observedProperty>'.
    '<observedProperty>42602 - NO2</observedProperty>'.
    '<observedProperty>42601 - NO</observedProperty>'.
    '<observedProperty>42101 - CO</observedProperty>'.
    '<observedProperty>81102 - PM10</observedProperty>';


$GetObservationStationTimeStationsBegin='<!-- station ID(s) -->'.
    '<featureOfInterest>';

$GetObservationStationTimeStationsEnd='</featureOfInterest>';
/*
    '<ObjectID>BETR222</ObjectID>'.
    '<ObjectID>BETR240</ObjectID>'.
  */


$GetObservationStationTimeFooter='<responseFormat>text/xml;subtype=&quot;om/1.0.0&quot;</responseFormat>'.
    '<resultModel>om:Measurement</resultModel>'.
    '<responseMode>inline</responseMode>'.
    '</GetObservation>';

error($ruta,'204',"Obtaining results from php://input");

$result = file_get_contents('php://input');

//$var=print_r($result,true);
//error($ruta, '20', $var);



error($ruta,'205',"Starting DB connection");

$conn_string = "host=51.254.136.244 port=5432 dbname=stc user=stc password=communithings";
$dbconn = pg_connect($conn_string);
$stat = pg_connection_status($dbconn);
if ($stat === PGSQL_CONNECTION_OK) {
    error($ruta,'501','CONNECTION OK');
}
else {
    error($ruta,'101','CONNECTION FAILED');
}


//updateStationsInfo::updateInfo($dbconn,$timeEnd);

/*
error($ruta,'206',"Checking date");

$datefile = "date.txt";
$gestordate = fopen($datefile, "r");
$lastUpdateDate = fgets($gestordate);
fclose($gestordate);
$currentdate = date("Y-m-d");


//Update stations id, name, position
if ($lastUpdateDate!=$currentdate) {

    error($ruta,'207',"Last date is not updated. Obtaining list of station in DB");

    //Obtenemos todas las estaciones existentes en la BD
    $stc_query = "TRUNCATE ct_stc.stations;";
    pg_query($dbconn, $stc_query);

    error($ruta,'208',"Station list obtained");

    //XML para conocer la ubicacion de las estaciones GetStation_BBOX
    $xml_getStations='<?xml version="1.0" encoding="UTF-8"?>
<GetFeatureOfInterest xmlns="http://www.opengis.net/sos/1.0"
  service="SOS" version="1.0.0"
  xmlns:ows="http://www.opengeospatial.net/ows"
  xmlns:gml="http://www.opengis.net/gml"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sos/1.0
  http://schemas.opengis.net/sos/1.0.0/sosGetFeatureOfInterest.xsd">
  
  <!-- bounding box for which stations are returned -->
  <location>
    <ogc:BBOX>
      <ogc:PropertyName>urn:ogc:data:location</ogc:PropertyName>
      <gml:Envelope srsName="urn:ogc:def:crs:EPSG::31370"><!-- Lambert xy -->
    <gml:lowerCorner>000000 000000</gml:lowerCorner><!-- Ymin Xmin -->
    <gml:upperCorner>250000 300000</gml:upperCorner><!-- Ymax Xmax -->
      </gml:Envelope>
    </ogc:BBOX>
  </location>
  
</GetFeatureOfInterest>';

    error($ruta,'209',"Calling curl to update id, name and position of stations");
    error($ruta,'209',$xml_getStations);

    //Llamada curl para obtener ubicacion de las estaciones
    $url = 'http://sos.irceline.be/sos';
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Expect:' ) );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml_getStations );

    $oXML = curl_exec($ch);
    error($ruta,'209',$oXML);

    if(curl_error($ch))
    {
        error($ruta,'102',"curl error: ". curl_error($ch));
    }

    error($ruta,'210',"curl executed. Parsing XML");

    $DOM = new DOMDocument('1.0', 'utf-8');
    $DOM->loadXML($oXML);
    $members = $DOM->getElementsByTagName('featureMember');
    //$resxml=$members->saveXML();
    //$cmember=count($members);
    //error ($ruta,'111',$resxml);

    //error($ruta,'211',"Updating stations info ".$cmember );

    //Actualizamos las estaciones en la BD con su id, name y point
    //Mientras haya members
    foreach($members as $member) {


        $stc_query = "select current_timestamp";
        $timestamp = pg_query($dbconn, $stc_query);
        $recvtime = $timeEnd;
        $stationID = utf8_decode($member->getElementsByTagName("SamplingPoint")->item(0)->attributes->item(0)->nodeValue);
        $stationname = utf8_decode($member->getElementsByTagName("name")->item(0)->nodeValue);
        $point = RemoveSpec( utf8_decode($member->getElementsByTagName("Point")->item(0)->nodeValue));
        $points = explode(" ", $point);
        $longitude = (string) $points[0];
        $latitude = (string) $points[1];

        $stc_query = "insert into ct_stc.stations (recvtime,stationid,stationname, position, longitude, latitude) values ('" . $recvtime . "', '" . $stationID . "', '" . $stationname . "', ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326), ST_X(ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326)),ST_Y(ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326)));";
        error($ruta,'212',"query para insertar: " .$stc_query);

        pg_query($dbconn, $stc_query);

    }//end foreach update stations

    error($ruta,'212',"Stations info updated. Overwritting update date");

    //Escribimos la fecha en el fichero date.txt
    error($ruta, '513', "Current Date: " . $currentdate);
    $gestordate = fopen($datefile, "w");
    fwrite($gestordate, $currentdate);
    fclose($gestordate);
}//end if currentdate != lastupdatedate
*/


//Propiedades del sensor
$stc_query = "select current_timestamp";
$timestamp = pg_query($dbconn, $stc_query);
$recvtime = $timeEnd;
$sensorID = "";
$sensorName = "";
$point = "";
$longitude = "";
$latitude = "";
$sensor_OFFSET_SERIAL = -1;
$sensor_OFFSET_BATTERY_LV = -1;
$sensor_OFFSET_TIMESTAMP = -1;
$sensor_OFFSET_LATITUDE = -1;
$sensor_OFFSET_LATITUDE_D1 = -1;
$sensor_OFFSET_LATITUDE_D2 = -1;
$sensor_OFFSET_LONGITUDE = -1;
$sensor_OFFSET_LONGITUDE_D1 = -1;
$sensor_OFFSET_LONGITUDE_D2 = -1;
$sensor_OFFSET_ALTITUDE = -1;
$sensor_OFFSET_ALTITUDE_D1 = -1;
$sensor_OFFSET_ALTITUDE_D2 = -1;
$sensor_OFFSET_SPEED = -1;
$sensor_OFFSET_SPEED_D1 = -1;
$sensor_OFFSET_SPEED_D2 = -1;
$sensor_OFFSET_TEMP_VAL = -1;
$sensor_OFFSET_PRESSURE_VAL = -1;
$sensor_OFFSET_HUMIDITY_VAL = -1;
$sensor_OFFSET_TEMP_CAL = -1;
$sensor_OFFSET_PRESSURE_CAL = -1;
$sensor_OFFSET_HUMIDITY_CAL = -1;
$sensor_OFFSET_CO_PPB = -1;
$sensor_OFFSET_O3_PPB = -1;
$sensor_OFFSET_NO2_PPB = -1;
$sensor_OFFSET_NH3_PPB = -1;
$sensor_OFFSET_PM10_PPB = -1;
$sensor_OFFSET_PM25_PPB = -1;
$sensor_OFFSET_PM1_PPB = -1;
$sensor_OFFSET_CO_PPB_CAL = -1;
$sensor_OFFSET_O3_PPB_CAL = -1;
$sensor_OFFSET_NO2_PPB_CAL = -1;
$sensor_OFFSET_NH3_PPB_CAL = -1;
$sensor_lora = -1;



error($ruta,'214',"Obtained results from php://input");
error($ruta,'215',"Decoding json");
//Parseamos datos del json del sensor
$result = file_get_contents('php://input');

$result=json_decode($result,true);
$var=print_r($result,true);
//error($ruta, '205', $var);


switch(json_last_error()) {
    case JSON_ERROR_NONE:
        echo ' - Sin errores';
        $msg='json_decode Sin errores';
        //              error($ruta,'501',$msg);
        break;
    case JSON_ERROR_DEPTH:
        echo ' - Excedido tamaño máximo de la pila';
        $msg='json_decode- Excedido tamaño máximo de la pila';
        //              error($ruta,'501',$msg);
        break;
    case JSON_ERROR_STATE_MISMATCH:
        echo ' - Desbordamiento de buffer o los modos no coinciden';
        $msg='json_decode- Desbordamiento de buffer o los modos no coinciden';
        //              error($ruta,'501',$msg);
        break;
    case JSON_ERROR_CTRL_CHAR:
        echo ' - Encontrado carácter de control no esperado';
        $msg='json_decode - Encontrado carácter de control no esperado';
        //              error($ruta,'501',$msg);
        break;
    case JSON_ERROR_SYNTAX:
        echo ' - Error de sintaxis, JSON mal formado';
        $msg='json_decode  - Error de sintaxis, JSON mal formado';
        //              error($ruta,'501',$msg);
        break;
    case JSON_ERROR_UTF8:
        echo ' - Caracteres UTF-8 malformados, posiblemente están mal codificados';
        $msg='json_decode - Caracteres UTF-8 malformados, posiblemente están mal codificados';
        //              error($ruta,'501',$msg);
        break;
    default:
        echo ' - Error desconocido';
        break;
}
echo PHP_EOL;


$var=$result["contextResponses"];
$var=$var[0];
$var=$var['contextElement'];
$attributes=$var['attributes'];
for ($i=0; $i<count($attributes);$i++){

    $attribute=$attributes[$i];
    switch ($attribute['name']) {
        case 'Altitude':
            $sensor_OFFSET_ALTITUDE = $attribute['value'];
            break;
        case 'Altitude_D1':
            $sensor_OFFSET_ALTITUDE_D1 = $attribute['value'];
            break;
        case 'Altitude_D2':
            $sensor_OFFSET_ALTITUDE_D2 = $attribute['value'];
            break;
        case 'BatteryLevel':
            $sensor_OFFSET_BATTERY_LV = $attribute['value'];
            break;
        case 'CO':
            $sensor_OFFSET_CO_PPB = $attribute['value'];
            break;
        case 'CO_CAL':
            $sensor_OFFSET_CO_PPB_CAL = $attribute['value'];
            break;
        case 'Humidity':
            $sensor_OFFSET_HUMIDITY_VAL = $attribute['value'];
            break;
        case 'HumidityCal':
            $sensor_OFFSET_HUMIDITY_CAL = $attribute['value'];
            break;
        case 'Latitude':
            $sensor_OFFSET_LATITUDE = $attribute['value'];
            break;
        case 'Latitude_D1':
            $sensor_OFFSET_LATITUDE_D1 = $attribute['value'];
            break;
        case 'Latitude_D2':
            $sensor_OFFSET_LATITUDE_D2 = $attribute['value'];
            break;
        case 'LoRa':
            $sensor_lora = $attribute['value'];
            break;
        case 'Longitude':
            $sensor_OFFSET_LONGITUDE = $attribute['value'];
            break;
        case 'Longitude_D1':
            $sensor_OFFSET_LONGITUDE_D1 = $attribute['value'];
            break;
        case 'Longitude_D2':
            $sensor_OFFSET_LONGITUDE_D2 = $attribute['value'];
            break;
        case 'NH3':
            $sensor_OFFSET_NH3_PPB = $attribute['value'];
            break;
        case 'NH3_CAL':
            $sensor_OFFSET_NH3_PPB_CAL = $attribute['value'];
            break;
        case 'NO2':
            $sensor_OFFSET_NO2_PPB = $attribute['value'];
            break;
        case 'NO2_CAL':
            $sensor_OFFSET_NO2_PPB_CAL = $attribute['value'];
            break;
        case 'O3':
            $sensor_OFFSET_O3_PPB = $attribute['value'];
            break;
        case 'O3_CAL':
            $sensor_OFFSET_O3_PPB_CAL = $attribute['value'];
            break;
        case 'PM1':
            $sensor_OFFSET_PM1_PPB = $attribute['value'];
            break;
        case 'PM10':
            $sensor_OFFSET_PM10_PPB = $attribute['value'];
            break;
        case 'PM25':
            $sensor_OFFSET_PM25_PPB = $attribute['value'];
            break;
        case 'Pressure':
            $sensor_OFFSET_PRESSURE_VAL = $attribute['value'];
            break;
        case 'PressureCal':
            $sensor_OFFSET_PRESSURE_CAL = $attribute['value'];
            break;
        case 'Serial':
            $sensor_OFFSET_SERIAL = $attribute['value'];
            break;
        case 'Speed':
            $sensor_OFFSET_SPEED = $attribute['value'];
            break;
        case 'Speed_D1':
            $sensor_OFFSET_SPEED_D1 = $attribute['value'];
            break;
        case 'Speed_D2':
            $sensor_OFFSET_SPEED_D2 = $attribute['value'];
            break;
        case 'Temperature':
            $sensor_OFFSET_TEMP_VAL = $attribute['value'];
            break;
        case 'TemperatureCal':
            $sensor_OFFSET_TEMP_CAL = $attribute['value'];
            break;
        case 'Timestamp':
            $sensor_OFFSET_TIMESTAMP = $attribute['value'];
            break;
        case 'lora':
            $sensor_lora = $attribute['value'];
            break;
        default:
            # code...
            break;
    }
}

error($ruta,'216',"json decoded");

$sensor_CO = -1;
$sensor_temperature = -1;
$sensor_NH3 = -1;
$sensor_NO = -1;
$sensor_NO2 = -1;
$sensor_O3 = -1;

if ($sensor_OFFSET_SERIAL == "403432100" || $sensor_OFFSET_SERIAL=="403373733") {
    $isNH3 = true;
}


if ($sensor_OFFSET_CO_PPB != -1)
{
    $sensor_CO = $sensor_OFFSET_CO_PPB;
}
elseif ($sensor_OFFSET_CO_PPB_CAL != -1)
{
    $sensor_CO = $sensor_OFFSET_CO_PPB_CAL;
}
if ($sensor_OFFSET_TEMP_CAL != -1)
{
    $sensor_temperature = $sensor_OFFSET_TEMP_CAL;
}
elseif ($sensor_OFFSET_TEMP_VAL != -1)
{
    $sensor_temperature = $sensor_OFFSET_TEMP_VAL;
}
if ($isNH3 && ($sensor_OFFSET_NH3_PPB != -1))
{
    $sensor_NH3 = $sensor_OFFSET_NH3_PPB;
}
elseif ($isNH3 && ($sensor_OFFSET_NH3_PPB_CAL != -1))
{
    $sensor_NH3 = $sensor_OFFSET_NH3_PPB;
}
elseif (!$isNH3 && ($sensor_OFFSET_NH3_PPB != -1))
{
    $sensor_NO = $sensor_OFFSET_NH3_PPB;
}
elseif (!$isNH3 && ($sensor_OFFSET_NH3_PPB_CAL != -1))
{
    $sensor_no = $sensor_OFFSET_NH3_PPB_CAL;
}
if ($sensor_OFFSET_O3_PPB != -1)
{
    $sensor_O3 = $sensor_OFFSET_O3_PPB;
}
elseif ($sensor_OFFSET_O3_PPB_CAL != -1)
{
    $sensor_O3 = $sensor_OFFSET_O3_PPB_CAL;
}
if ($sensor_OFFSET_NO2_PPB != -1)
{
    $sensor_NO2 = $sensor_OFFSET_NO2_PPB;
}
elseif ($sensor_OFFSET_NO2_PPB_CAL != -1)
{
    $sensor_NO2 = $sensor_OFFSET_NO2_PPB_CAL;
}

error($ruta,'217',"Obtaining station list in a radius of 500m around the sensor");

//Obtenemos las estaciones dentro de un radio
$query = "SELECT stationid FROM ct_stc.stations WHERE ST_Distance_Sphere(position, ST_SetSRID(ST_MakePoint(" . $sensor_OFFSET_LONGITUDE . "," . $sensor_OFFSET_LATITUDE . "),4326)) <= ".$distanceToStations.";";
//$query = "SELECT stationid FROM ct_stc.stations;";
$stationsNearList = pg_query($dbconn, $query);
error($ruta,'218',$query);


//update measures stations
if (pg_num_rows($stationsNearList)!=0)
{
    error($ruta,'219',"Station list around the sensor not empty");

    error($ruta,'220',"Obtaining measures for stations around the sensor");


    for ($i = 0; $i<count($stationsMeasuresName);$i++)
    {

        error($ruta,'221',"Obtaining measures for loop number: ".$i.' of ' .count($stationsMeasuresName) .' Measure name = '.$stationsMeasuresName[$i]);

        //XML para conocer las medidas de las estaciones GetObs_station_BBOX
        $xml_obs_stations='<?xml version="1.0" encoding="UTF-8"?>
            <GetObservation xmlns="http://www.opengis.net/sos/1.0"
              xmlns:ows="http://www.opengis.net/ows/1.1"
              xmlns:gml="http://www.opengis.net/gml"
              xmlns:ogc="http://www.opengis.net/ogc"
              xmlns:om="http://www.opengis.net/om/1.0"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://www.opengis.net/sos/1.0
              http://schemas.opengis.net/sos/1.0.0/sosGetObservation.xsd"
              service="SOS" version="1.0.0" srsName="urn:ogc:def:crs:EPSG::4326">
            
              <!-- offering ID -->
              <offering>'.$stationsMeasuresName[$i].'</offering>
            
              <!-- time interval -->
                '.$GetObservationStationTimeTimeInterval.'
            
              <!-- saroad code -->
              <observedProperty>'.$stationsMeasuresName[$i].'</observedProperty>
            
              <!-- station ID(s) -->
              <featureOfInterest>';

        for ($j = 0; $j<pg_num_rows($stationsNearList);$j++)
        {

            $xml_obs_stations= $xml_obs_stations.'<ObjectID>';
            $stationsidfetched = pg_fetch_row($stationsNearList, $j)[0];

            $xml_obs_stations = $xml_obs_stations. $stationsidfetched;
            $xml_obs_stations= $xml_obs_stations.'</ObjectID>';
        }

        $xml_obs_stations = $xml_obs_stations.'</featureOfInterest>
            
              <responseFormat>text/xml;subtype=&quot;om/1.0.0&quot;</responseFormat>
              <resultModel>om:Measurement</resultModel>
              <responseMode>inline</responseMode>
            </GetObservation>';

        error($ruta,'222',"XML: ".$xml_obs_stations);

        //Llamada curl para obtener las medidas de las estaciones
        $url = 'http://sos.irceline.be/sos';
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Expect:' ) );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml_obs_stations );
        error($ruta,'222',"Antes de exec");
        $oXML = curl_exec($ch);
        $res=curl_getinfo($ch, CURLINFO_HTTP_CODE);
        error($ruta,'223',$res);
        error($ruta,'224',$oXML);

        if(curl_error($ch))
        {
            error($ruta,'103',"curl error: ". curl_error($ch));
        }

        error($ruta,'225',"Parsing measures for stations around the sensor to XML");

        $DOM = new DOMDocument('1.0', 'utf-8');
        $DOM->loadXML($oXML);
        $members = $DOM->getElementsByTagName('member');

        //Obtenemos observacion de las estaciones dentro del radio
        //Mientras haya members
        foreach($members as $member) {

            $isInapplicable = false;
            if (strpos($oXML,'urn:ogc:def:nil:OGC:inapplicable')) {
                $isInapplicable = true;
            }

            error($ruta, '227', "Is inapplicable?: " . $isInapplicable);

            if (!$isInapplicable) {


                $stc_query = "select current_timestamp";
                $timestamp = pg_query($dbconn, $stc_query);
                $recvtime_array = $timeEnd;
                $recvtime=$recvtime_array;
                $stationID = utf8_decode($member->getElementsByTagName("featureOfInterest")->item(0)->attributes->item(0)->nodeValue);
                $stationname = utf8_decode($member->getElementsByTagName("name")->item(0)->nodeValue);
                $measure = utf8_decode($member->getElementsByTagName("result")->item(0)->nodeValue);
                $point = RemoveSpec(utf8_decode($member->getElementsByTagName("Point")->item(0)->nodeValue));
                $points = explode(" ", $point);
                $longitude = (string)$points[0];
                $latitude = (string)$points[1];
                $measurements = $member->getElementsByTagName('Measurement');


                $OFFSET_CO_PPB = -1;
                $OFFSET_NO_PPB = -1;
                $OFFSET_NO2_PPB = -1;
                $OFFSET_O3_PPB = -1;
                $OFFSET_PM1_PPB = -1;
                $OFFSET_PM10_PPB = -1;
                $OFFSET_PM25_PPB = -1;
                $OFFSET_TEMPERATURE_PPB = -1;
                $OFFSET_WINDSPEED_PPB = -1;

                error($ruta, '228', "Checking measures to send calibrate process if neccesary");

                foreach ($measurements as $measurement) {
                    $observedProperty = utf8_decode($measurement->getElementsByTagName("observedProperty")->item(0)->attributes->item(0)->nodeValue);
                    $result_measure = utf8_decode($measurement->getElementsByTagName("result")->item(0)->nodeValue);

                    switch ($observedProperty) {
                        case '42101 - CO':
                            $OFFSET_CO_PPB = $result_measure;
                            if (!((($sensor_CO != -1)) &&
                                ((($OFFSET_CO_PPB * (1 - $error_percentage_measures)) < $sensor_CO) &&
                                    ($sensor_CO < ($OFFSET_CO_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '42601 - NO':
                            if (!$isNH3) {
                                $OFFSET_NO_PPB = $result_measure;
                                if (!(($sensor_NO != -1) &&
                                    ((($OFFSET_NO_PPB * (1 - $error_percentage_measures)) < $sensor_NO) &&
                                        ($sensor_NO < ($OFFSET_NO_PPB * (1 + $error_percentage_measures)))))
                                )
                                    $recalibrate = true;
                            }
                            break;
                        case '42602 - NO2':
                            $OFFSET_NO2_PPB = $result_measure;
                            if (!(($sensor_NO2 != -1) &&
                                ((($OFFSET_NO2_PPB * (1 - $error_percentage_measures)) < $sensor_NO2) &&
                                    ($sensor_NO2 < ($OFFSET_NO2_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '44201 - O3':
                            $OFFSET_O3_PPB = $result_measure;
                            if (!(($sensor_O3 != -1) &&
                                ((($OFFSET_O3_PPB * (1 - $error_percentage_measures)) < $sensor_O3) &&
                                    ($sensor_O3 < ($OFFSET_O3_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '89101 - PM1':
                            $OFFSET_PM1_PPB = $result_measure;
                            if (!(($sensor_OFFSET_PM1_PPB != -1) &&
                                ((($OFFSET_PM1_PPB * (1 - $error_percentage_measures)) < $sensor_OFFSET_PM1_PPB) &&
                                    ($sensor_OFFSET_PM1_PPB < ($OFFSET_PM1_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '81102 - PM10':
                            $OFFSET_PM10_PPB = $result_measure;
                            if (!(($sensor_OFFSET_PM10_PPB != -1) &&
                                ((($OFFSET_PM10_PPB * (1 - $error_percentage_measures)) < $sensor_OFFSET_PM10_PPB) &&
                                    ($sensor_OFFSET_PM10_PPB < ($OFFSET_PM10_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '81104 - PM2.5':
                            $OFFSET_PM25_PPB = $result_measure;
                            if (!(($sensor_OFFSET_PM25_PPB != -1) &&
                                ((($OFFSET_PM25_PPB * (1 - $error_percentage_measures)) < $sensor_OFFSET_PM25_PPB) &&
                                    ($sensor_OFFSET_PM25_PPB < ($OFFSET_PM25_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '62101 - TT':
                            $OFFSET_TEMPERATURE_PPB = $result_measure;
                            if (!(($sensor_temperature != -1) &&
                                ((($OFFSET_TEMPERATURE_PPB * (1 - $error_percentage_measures)) < $sensor_temperature) &&
                                    ($sensor_temperature < ($OFFSET_TEMPERATURE_PPB * (1 + $error_percentage_measures)))))
                            )
                                $recalibrate = true;
                            break;
                        case '61110 - WSP-SCA':
                            $OFFSET_WINDSPEED_PPB = $result_measure;
                            break;
                        default:
                            # code...
                            break;
                    }//end switch
                }
                error($ruta, '229', "Inserting data in DB");

                $query = "INSERT INTO ct_stc.calibrations(recvtime,stationid,stationname, position, longitude, latitude, position_measurement, lon_measurement, lat_measurement,  serial_measurement, temp_base, no_base, no2_base, co_base, o3_base, pm1_base, pm25_base, pm10_base, wsp_sca_base, temp_measurement, no_measurement, no2_measurement, co_measurement, o3_measurement, pm1_measurement, pm25_measurement, pm10_measurement, nh3_measurement) VALUES ('" . $recvtime . "', '" . $stationID . "', '" . $stationname . "', ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326), ST_X(ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326)),ST_Y(ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326)), ST_SetSRID(ST_MakePoint(" . $sensor_OFFSET_LONGITUDE . "," . $sensor_OFFSET_LATITUDE . "), 4326),ST_X(ST_SetSRID(ST_MakePoint(" . $sensor_OFFSET_LONGITUDE . "," . $sensor_OFFSET_LATITUDE . "), 4326)),ST_Y(ST_SetSRID(ST_MakePoint(" . $sensor_OFFSET_LONGITUDE . "," . $sensor_OFFSET_LATITUDE . "), 4326)),'" . $sensor_OFFSET_SERIAL . "','" . $OFFSET_TEMPERATURE_PPB . "','" . $OFFSET_NO_PPB . "','" . $OFFSET_NO2_PPB . "','" . $OFFSET_CO_PPB . "','" . $OFFSET_O3_PPB . "','" . $OFFSET_PM1_PPB . "','" . $OFFSET_PM25_PPB . "','" . $OFFSET_PM10_PPB . "','" . $OFFSET_WINDSPEED_PPB . "','" . $sensor_temperature . "','" . $sensor_NO . "','" . $sensor_NO2 . "','" . $sensor_CO . "','" . $sensor_O3 . "','" . $sensor_OFFSET_PM1_PPB . "','" . $sensor_OFFSET_PM25_PPB . "','" . $sensor_OFFSET_PM10_PPB . "','" . $sensor_NH3 . "');";
                //error($ruta, '230', "Query: " . $query);

                pg_query($dbconn, $query);
            }
        }//end for each member


    }

    error($ruta,'231',"Checking measures of all stations ended");

}//end if (count (stations near) !=0)
else
{
    error($ruta,'232',"Station list around the sensor is EMPTY");
}


if($recalibrate)
{
    error($ruta,'233',"Starting calibration proccess");
    //TODO mandar recalibración
}

error($ruta,'299',"End Calibrator");


/**
 * Deletes all non printable characters
 * @param $str
 * @return mixed|string
 */
function RemoveSpec($str){
    $str = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str);
    $str = trim($str);

    return $str;
}


?>