<?php
//include "log.php";
//$ruta='../../../../tmp/errorSTCDB.log';
//error($ruta,'206',"Start stc_db_iMinds");

date_default_timezone_set('Europe/Brussels');
    $result = file_get_contents('php://input');
    
        $result=json_decode($result,true);
        $var=print_r($result,true);
        error($ruta, '205', $var);
        switch(json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - Sin errores';
                        $msg='json_decode Sin errores';
//              error($ruta,'501',$msg);
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Excedido tamaño máximo de la pila';
                        $msg='json_decode- Excedido tamaño máximo de la pila';
//              error($ruta,'501',$msg);
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Desbordamiento de buffer o los modos no coinciden';
                        $msg='json_decode- Desbordamiento de buffer o los modos no coinciden';
//              error($ruta,'501',$msg);
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Encontrado carácter de control no esperado';
                        $msg='json_decode - Encontrado carácter de control no esperado';
//              error($ruta,'501',$msg);
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Error de sintaxis, JSON mal formado';
                        $msg='json_decode  - Error de sintaxis, JSON mal formado';
//              error($ruta,'501',$msg);
        break;
        case JSON_ERROR_UTF8:
            echo ' - Caracteres UTF-8 malformados, posiblemente están mal codificados';
                        $msg='json_decode - Caracteres UTF-8 malformados, posiblemente están mal codificados';
//              error($ruta,'501',$msg);
        break;
        default:
            echo ' - Error desconocido';
        break;
    }
    echo PHP_EOL;
    
    //$var=print_r($result,true);
        //error($ruta, '20', $var);

    //
    

     $OFFSET_SERIAL = -1;
     $OFFSET_BATTERY_LV= -1;
     $OFFSET_TIMESTAMP= -1;
     $OFFSET_LATITUDE= -1;
     $OFFSET_LATITUDE_D1= -1;
     $OFFSET_LATITUDE_D2= -1;
     $OFFSET_LONGITUDE= -1;
     $OFFSET_LONGITUDE_D1= -1;
     $OFFSET_LONGITUDE_D2= -1;
     $OFFSET_ALTITUDE= -1;
     $OFFSET_ALTITUDE_D1= -1;
     $OFFSET_ALTITUDE_D2= -1;
     $OFFSET_SPEED= -1;
     $OFFSET_SPEED_D1= -1;
     $OFFSET_SPEED_D2 = -1;
     $OFFSET_TEMP_VAL= -1;
     $OFFSET_PRESSURE_VAL= -1;
     $OFFSET_HUMIDITY_VAL= -1;
     $OFFSET_TEMP_CAL= -1;
     $OFFSET_PRESSURE_CAL= -1;
     $OFFSET_HUMIDITY_CAL= -1;
     $OFFSET_CO_PPB= -1;
     $OFFSET_O3_PPB= -1;
     $OFFSET_NO2_PPB= -1;
     $OFFSET_NH3_PPB= -1;
     $OFFSET_PM10_PPB= -1;
     $OFFSET_PM25_PPB= -1;
     $OFFSET_PM1_PPB= -1;
     $OFFSET_CO_PPB_CAL= -1;
     $OFFSET_O3_PPB_CAL= -1;
     $OFFSET_NO2_PPB_CAL= -1;
     $OFFSET_NH3_PPB_CAL= -1;
     $lora= -1;
    

    $var=$result["contextResponses"];
    $var=$var[0];
    $var=$var['contextElement'];
    $attributes=$var['attributes'];
    for ($i=0; $i<count($attributes);$i++){

        $attribute=$attributes[$i];
        //$attribute=$attribute['name'];
        //error($ruta,'206',"Attribute name ".$attribute['name']);
        //error($ruta,'206',"Attribute value ".$attribute['value']);
        switch ($attribute['name']) {
            case 'Altitude':
                $OFFSET_ALTITUDE = $attribute['value'];
                break;
            case 'Altitude_D1':
                $OFFSET_ALTITUDE_D1 = $attribute['value'];
                break;
            case 'Altitude_D2':
                $OFFSET_ALTITUDE_D2 = $attribute['value'];
                break;
            case 'BatteryLevel':
                $OFFSET_BATTERY_LV = $attribute['value'];
                break;
            case 'CO':
                $OFFSET_CO_PPB = $attribute['value'];
                break;
            case 'CO_CAL':
                $OFFSET_CO_PPB_CAL = $attribute['value'];
                break;
             case 'Humidity':
                $OFFSET_HUMIDITY_VAL = $attribute['value'];
                break;
            case 'HumidityCal':
                $OFFSET_HUMIDITY_CAL = $attribute['value'];
                break;
            case 'Latitude':
                $OFFSET_LATITUDE = $attribute['value'];
                break;
            case 'Latitude_D1':
                $OFFSET_LATITUDE_D1 = $attribute['value'];
                break;
            case 'Latitude_D2':
                $OFFSET_LATITUDE_D2 = $attribute['value'];
                break;
            case 'LoRa':
                $lora = $attribute['value'];
                break;
            case 'Longitude':
                $OFFSET_LONGITUDE = $attribute['value'];
                break;
            case 'Longitude_D1':
                $OFFSET_LONGITUDE_D1 = $attribute['value'];
                break;
             case 'Longitude_D2':
                $OFFSET_LONGITUDE_D2 = $attribute['value'];
                break;
            case 'NH3':
                $OFFSET_NH3_PPB = $attribute['value'];
                break;
            case 'NH3_CAL':
                $OFFSET_NH3_PPB_CAL = $attribute['value'];
                break;
            case 'NO2':
                $OFFSET_NO2_PPB = $attribute['value'];
                break;
            case 'NO2_CAL':
                $OFFSET_NO2_PPB_CAL = $attribute['value'];
                break;
            case 'O3':
                $OFFSET_O3_PPB = $attribute['value'];
                break;
            case 'O3_CAL':
                $OFFSET_O3_PPB_CAL = $attribute['value'];
                break;
            case 'PM1':
                $OFFSET_PM1_PPB = $attribute['value'];
                break;
             case 'PM10':
                $OFFSET_PM10_PPB = $attribute['value'];
                break;
            case 'PM25':
                $OFFSET_PM25_PPB = $attribute['value'];
                break;
            case 'Pressure':
                $OFFSET_PRESSURE_VAL = $attribute['value'];
                break;
            case 'PressureCal':
                $OFFSET_PRESSURE_CAL = $attribute['value'];
                break;
            case 'Serial':
                $OFFSET_SERIAL = $attribute['value'];
                break;
            case 'Speed':
                $OFFSET_SPEED = $attribute['value'];
                break;
            case 'Speed_D1':
                $OFFSET_SPEED_D1 = $attribute['value'];
                break;
            case 'Speed_D2':
                $OFFSET_SPEED_D2 = $attribute['value'];
                break;
             case 'Temperature':
                $OFFSET_TEMP_VAL = $attribute['value'];
                break;
            case 'TemperatureCal':
                $OFFSET_TEMP_CAL = $attribute['value'];
                break;
            case 'Timestamp':
                $OFFSET_TIMESTAMP = $attribute['value'];
                break;
            case 'lora':
                $lora = $attribute['value'];
                break;
            default:
                # code...
                break;
        }
    }
    
    $conn_string = "host=51.254.136.244 port=5432 dbname=stc user=stc password=communithings";
    $dbconn = pg_connect($conn_string);
    $stat = pg_connection_status($dbconn);
    if ($stat === PGSQL_CONNECTION_OK) {
            error($ruta,'501','CONNECTION OK');
    } else {
            error($ruta,'501','CONNECTION FAILED');
    }  
    // InserT
    if ($OFFSET_SERIAL == "403432100" || $OFFSET_SERIAL=="403373733"){
    $stc_query="INSERT into ct_stc.def_servpath (recvtime,fiwareservicepath,entityid,entitytype,
        altitude,altitude_md,
        altitude_d1,altitude_d1_md,
        altitude_d2,altitude_d2_md,
        batterylevel,batterylevel_md,
        co,co_md,
        co_cal,co_cal_md,
        humidity,humidity_md,humiditycal,humiditycal_md,
        latitude,latitude_md,
        latitude_d1,Latitude_d1_md,
        latitude_d2,Latitude_d2_md,
        longitude,longitude_md,
        longitude_d1,longitude_d1_md,
        longitude_d2,longitude_d2_md,
        nh3,nh3_md,nh3_cal,nh3_cal_md,
        no2,no2_md,no2_cal,no2_cal_md,
        o3,o3_md,   o3_cal,o3_cal_md,
        pm1,pm1_md,pm10,pm10_md,pm25,pm25_md,
        position,position_md,pressure,pressure_md,pressurecal,pressurecal_md,
        serial,serial_md,
        speed,speed_md,
        speed_d1,speed_d1_md,
        speed_d2,speed_d2_md,
        temperature,temperature_md,temperaturecal,temperaturecal_md,lora,lora_md,
        timestamp_1,timestamp_md) values ('".date("Y-m-d H:i:s")."','def_servpath','".$OFFSET_SERIAL."','STCSensor',
'".$OFFSET_ALTITUDE."','[]',
'".$OFFSET_ALTITUDE_D1."','[]',
'".$OFFSET_ALTITUDE_D2."','[]',
'".$OFFSET_BATTERY_LV."','[]',
'".$OFFSET_CO_PPB."','[]',
'".$OFFSET_CO_PPB_CAL."','[]',
'".$OFFSET_HUMIDITY_VAL."','[]',
'".$OFFSET_HUMIDITY_CAL."','[]',
'".$OFFSET_LATITUDE."','[]',
'".$OFFSET_LATITUDE_D1."','[]',
'".$OFFSET_LATITUDE_D2."','[]',
'".$OFFSET_LONGITUDE."','[]',
'".$OFFSET_LONGITUDE_D1."','[]',
'".$OFFSET_LONGITUDE_D2."','[]',
'".$OFFSET_NH3_PPB."','[]',
'".$OFFSET_NH3_PPB_CAL."','[]',
'".$OFFSET_NO2_PPB."','[]',
'".$OFFSET_NO2_PPB_CAL."','[]',
'".$OFFSET_O3_PPB."','[]',
'".$OFFSET_O3_PPB_CAL."','[]',
'".$OFFSET_PM1_PPB."','[]',
'".$OFFSET_PM10_PPB."','[]',
'".$OFFSET_PM25_PPB."','[]',
ST_SetSRID(ST_MakePoint(".$OFFSET_LONGITUDE.",".$OFFSET_LATITUDE."), 4326),'[]',
'".$OFFSET_PRESSURE_VAL."','[]',
'".$OFFSET_PRESSURE_CAL."','[]',
'".$OFFSET_SERIAL."','[]',
'".$OFFSET_SPEED."','[]',
'".$OFFSET_SPEED_D1."','[]',
'".$OFFSET_SPEED_D2."','[]',
'".$OFFSET_TEMP_VAL."','[]',
'".$OFFSET_TEMP_CAL."','[]',
'".$lora."','[]',
'".$OFFSET_TIMESTAMP."','[]')";
    error($ruta,'501NH3',$stc_query);
}
else{
    $stc_query="INSERT into ct_stc.def_servpath (recvtime,fiwareservicepath,entityid,entitytype,
        altitude,altitude_md,
        altitude_d1,altitude_d1_md,
        altitude_d2,altitude_d2_md,
        batterylevel,batterylevel_md,
        co,co_md,
        co_cal,co_cal_md,
        humidity,humidity_md,humiditycal,humiditycal_md,
        latitude,latitude_md,
        latitude_d1,Latitude_d1_md,
        latitude_d2,Latitude_d2_md,
        longitude,longitude_md,
        longitude_d1,longitude_d1_md,
        longitude_d2,longitude_d2_md,
        nox,nox_md,nox_cal,nox_cal_md,
        no2,no2_md,no2_cal,no2_cal_md,
        o3,o3_md,   o3_cal,o3_cal_md,
        pm1,pm1_md,pm10,pm10_md,pm25,pm25_md,
        position,position_md,pressure,pressure_md,pressurecal,pressurecal_md,
        serial,serial_md,
        speed,speed_md,
        speed_d1,speed_d1_md,
        speed_d2,speed_d2_md,
        temperature,temperature_md,temperaturecal,temperaturecal_md,lora,lora_md,
        timestamp_1,timestamp_md) values ('".date("Y-m-d H:i:s")."','def_servpath','".$OFFSET_SERIAL."','STCSensor',
'".$OFFSET_ALTITUDE."','[]',
'".$OFFSET_ALTITUDE_D1."','[]',
'".$OFFSET_ALTITUDE_D2."','[]',
'".$OFFSET_BATTERY_LV."','[]',
'".$OFFSET_CO_PPB."','[]',
'".$OFFSET_CO_PPB_CAL."','[]',
'".$OFFSET_HUMIDITY_VAL."','[]',
'".$OFFSET_HUMIDITY_CAL."','[]',
'".$OFFSET_LATITUDE."','[]',
'".$OFFSET_LATITUDE_D1."','[]',
'".$OFFSET_LATITUDE_D2."','[]',
'".$OFFSET_LONGITUDE."','[]',
'".$OFFSET_LONGITUDE_D1."','[]',
'".$OFFSET_LONGITUDE_D2."','[]',
'".$OFFSET_NH3_PPB."','[]',
'".$OFFSET_NH3_PPB_CAL."','[]',
'".$OFFSET_NO2_PPB."','[]',
'".$OFFSET_NO2_PPB_CAL."','[]',
'".$OFFSET_O3_PPB."','[]',
'".$OFFSET_O3_PPB_CAL."','[]',
'".$OFFSET_PM1_PPB."','[]',
'".$OFFSET_PM10_PPB."','[]',
'".$OFFSET_PM25_PPB."','[]',
ST_SetSRID(ST_MakePoint(".$OFFSET_LONGITUDE.",".$OFFSET_LATITUDE."), 4326),'[]',
'".$OFFSET_PRESSURE_VAL."','[]',
'".$OFFSET_PRESSURE_CAL."','[]',
'".$OFFSET_SERIAL."','[]',
'".$OFFSET_SPEED."','[]',
'".$OFFSET_SPEED_D1."','[]',
'".$OFFSET_SPEED_D2."','[]',
'".$OFFSET_TEMP_VAL."','[]',
'".$OFFSET_TEMP_CAL."','[]',
'".$lora."','[]',
'".$OFFSET_TIMESTAMP."','[]')";
    error($ruta,'501Nox',$stc_query);
}
    $result_query = pg_query($dbconn, $stc_query);
    if (!$result_query) {
        error($ruta,'501',"PSQL query. An error occurred.");
    }
?>


