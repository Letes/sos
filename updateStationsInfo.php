<?php
//phpinfo();


include "log.php";

        $ruta = '../../../../tmp/errorUpdateStations.log';
        error($ruta, '201', "Start Calibrator");
        $timeEnd = date('c');


        $conn_string = "host=51.254.136.244 port=5432 dbname=stc user=stc password=communithings";
        $dbconn = pg_connect($conn_string);
        $stat = pg_connection_status($dbconn);
        if ($stat === PGSQL_CONNECTION_OK) {
            error($ruta, '501', 'CONNECTION OK');
        } else {
            error($ruta, '101', 'CONNECTION FAILED');
        }


        error($ruta, '206', "Checking date");

        $datefile = "date.txt";
        $gestordate = fopen($datefile, "r");
        $lastUpdateDate = fgets($gestordate);
        fclose($gestordate);
        $currentdate = date("Y-m-d");

        //Update stations id, name, position
        if ($lastUpdateDate != $currentdate) {

            error($ruta, '207', "Last date is not updated. Obtaining list of station in DB");

            //XML para conocer la ubicacion de las estaciones GetStation_BBOX
            $xml_getStations = '<?xml version="1.0" encoding="UTF-8"?>
                <GetFeatureOfInterest xmlns="http://www.opengis.net/sos/1.0"
                  service="SOS" version="1.0.0"
                  xmlns:ows="http://www.opengeospatial.net/ows"
                  xmlns:gml="http://www.opengis.net/gml"
                  xmlns:ogc="http://www.opengis.net/ogc"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:schemaLocation="http://www.opengis.net/sos/1.0
                  http://schemas.opengis.net/sos/1.0.0/sosGetFeatureOfInterest.xsd">
                  
                  <!-- bounding box for which stations are returned -->
                  <location>
                    <ogc:BBOX>
                      <ogc:PropertyName>urn:ogc:data:location</ogc:PropertyName>
                      <gml:Envelope srsName="urn:ogc:def:crs:EPSG::31370"><!-- Lambert xy -->
                    <gml:lowerCorner>000000 000000</gml:lowerCorner><!-- Ymin Xmin -->
                    <gml:upperCorner>250000 300000</gml:upperCorner><!-- Ymax Xmax -->
                      </gml:Envelope>
                    </ogc:BBOX>
                  </location>
                  
                </GetFeatureOfInterest>';

            error($ruta, '209', "Calling curl to update id, name and position of stations");
            //error($ruta, '209', $xml_getStations);

            //Llamada curl para obtener ubicacion de las estaciones
            $url = 'http://sos.irceline.be/sos';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_getStations);

            $oXML = curl_exec($ch);
            //error($ruta, '209', $oXML);

            if (curl_error($ch)) {
                error($ruta, '102', "curl error: " . curl_error($ch));
            }

            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            error($ruta, '102', "curl executed. HTTP code: " . $http_code);

            if($http_code == 200)
            {
                error($ruta, '102', "Estamos dentro de if code == 200");

                //Truncamos ct_stc.stations BD
                $stc_query = "TRUNCATE ct_stc.stations;";
                pg_query($dbconn, $stc_query);
                error($ruta,'210',"Parsing XML");

                $DOM = new DOMDocument('1.0', 'utf-8');
                $DOM->loadXML($oXML);
                $members = $DOM->getElementsByTagName('featureMember');
                error($ruta,'210',"XML Parsed");

                //Actualizamos las estaciones en la BD con su id, name y point
                //Mientras haya members

                foreach ($members as $member) {
                    $recvtime = $timeEnd;
                    error($ruta,'210',"Recvtime: ". $recvtime);

                    $stationID = utf8_decode($member->getElementsByTagName("SamplingPoint")->item(0)->attributes->item(0)->nodeValue);
                    error($ruta,'210',"stationID: ". $stationID);

                    $stationname = utf8_decode($member->getElementsByTagName("name")->item(0)->nodeValue);
                    error($ruta,'210',"stationname: ". $stationname);

                    $point = RemoveSpec(utf8_decode($member->getElementsByTagName("Point")->item(0)->nodeValue));
                    var_dump($point, true);
                    $points = explode(" ", $point);
                    $longitude = (string)$points[0];
                    error($ruta,'210',"longitude: ". $longitude);

                    $latitude = (string)$points[1];
                    error($ruta,'210',"latitude: ". $latitude);


                    $stc_query = "INSERT INTO ct_stc.stations (recvtime,stationid,stationname, position, longitude, latitude) VALUES ('" . $recvtime . "', '" . $stationID . "', '" . $stationname . "', ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326), ST_X(ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326)),ST_Y(ST_Transform(ST_SetSRID(ST_MakePoint(" . $longitude . "," . $latitude . "), 31370),4326)));";
                    error($ruta, '212', "query para insertar: " . $stc_query);

                    pg_query($dbconn, $stc_query);
                }//end foreach update stations

                error($ruta, '212', "Stations info updated. Overwritting update date");

                //Escribimos la fecha en el fichero date.txt
                error($ruta, '513', "Current Date: " . $currentdate);
                $gestordate = fopen($datefile, "w");
                fwrite($gestordate, $currentdate);
                fclose($gestordate);

            }//end if HTTP code == 200
            else
            {
                error($ruta, '102', "No estamos dentro de if code == 200");
            }

        }//end if currentdate != lastupdatedate
        else
        {
            error($ruta, '513', "Date is updated. Current date: " . $currentdate);
        }


/**
 * Deletes all non printable characters
 * @param $str
 * @return mixed|string
 */
function RemoveSpec($str){
    $str = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str);
    $str = trim($str);

    return $str;
}